from fastapi import FastAPI
from schema import *
from db import model
from db.database import engine
from router import user, webhook
from utils import authentication
from fastapi.responses import HTMLResponse
import codecs
from fastapi.middleware.cors import CORSMiddleware


app = FastAPI()

list_router = [
    user.router,
    authentication.router,
    webhook.router
]

@app.get("/", response_class=HTMLResponse)
async def read_root():
    with codecs.open("index.html", "r", "utf-8") as file:
        return file.read()

for router in list_router: 
      app.include_router(router,prefix="/run_be") 

model.Base.metadata.create_all(bind=engine)

origins = [
    'http://localhost:3000'
]

app.add_middleware(
    CORSMiddleware,
    allow_origins = origins,
    allow_credentials = True,
    allow_methods = ["*"],
    allow_headers = ["*"]
)
